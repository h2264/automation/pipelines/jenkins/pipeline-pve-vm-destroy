def tf_hash = [:]
pipeline {
    agent {
        node {
            label 'drone'
        }
    }
    
    environment {
        PVE_CRED=credentials('internal_pve_credentials')
        REPO_DIR="repo"
        BOOTSRAP_HASH_PASS=credentials('internal_bootstrapp_credentials')
        TF_VARS="TF_VAR_pve_password='${PVE_CRED_PSW}' TF_VAR_template_password='${BOOTSRAP_HASH_PASS_PSW}'"
        workspace = "${WORKSPACE}"
        REALM = "FWC.DC.OPS.VM"
    }
    
    parameters {
        string(
            name: 'GIT_REPO',
            defaultValue: '',
            description: ''
        )
        string(
            name: '_SLACK_CHANNEL',
            defaultValue: 'jenkins-pipeline-pve-vm-destroy',
            description: 'Slack channel to receive notifications'
        )        
        credentials credentialType: 'com.datapipe.jenkins.vault.credentials.common.VaultSSHUserPrivateKeyImpl',
            defaultValue: 'VAULT_JENKINS_GITLAB_SSHKEY',
            name: 'GIT_CREDS', 
            required: true
    }

    options {
        ansiColor('gnome-terminal')
        buildDiscarder(logRotator(daysToKeepStr: '7', numToKeepStr: '20'))
    }

    stages {
        stage('Clean repo folder') {
            steps {
                dir(workspace) {
                    script {
                        sh """
                        rm -rf ${REPO_DIR}
                        """
                    }
                }
            }
        }

        stage('Clone') {
            environment {
                GIT_SSH_COMMAND = "ssh -o StrictHostKeyChecking=no"
            }
            steps {
                dir(workspace) {
                    sshagent([GIT_CREDS.toString()]) {
                        script {
                            sh """
                            git clone ${GIT_REPO} ${REPO_DIR}
                            """
                        }
                    }
                }
            }
        }

        stage('Init') {
            steps {
                dir("${workspace}/${REPO_DIR}") {
                    script {
                        sh """
                        ${TF_VARS} terraform init
                        """
                    }
                }
                
            }
        }

        stage('Validate') {
            steps {
                dir("${workspace}/${REPO_DIR}") {
                    script {                    
                        sh """
                        terraform validate
                        """
                    }
                }
            }
        }

        stage('DNS Resignation') {
            steps {
                dir("${workspace}/${REPO_DIR}") {
                    script {
                        catchError(buildResult: 'SUCCESS', stageResult: 'ABORTED') {
                            // OBTAIN TERRAFORM OUTPUT
                            _TF_IPV4 = sh(
                                script: "terraform output --json instance_ipv4_addr | jq -r '.[] | .[] '",
                                returnStdout: true
                            ).split("\n")
                            _TF_HOSTNAME = sh(
                                script: "terraform output --json instance_hostname | jq -r '.[] | .[] '",
                                returnStdout: true
                            ).split("\n")
                            _TF_DOMAIN = sh(
                                script: "terraform output --json instance_domainname | jq -r '.[] | .[] '",
                                returnStdout: true
                            ).split("\n")
                            // LOOP TERRAFORM OUTPUT
                            def branches = [:]
                            for (int i=0; i<_TF_IPV4.size();i++) {
                                def _HOSTNAME   = _TF_HOSTNAME[i]
                                def _IPv4       = _TF_IPV4[i]
                                def _DOMAIN     = _TF_DOMAIN[i]
                                def _REALM      = env.REALM
                                try {
                                    branches["${_HOSTNAME}.${_DOMAIN}"] = {
                                        sh("nslookup ${_HOSTNAME}.${_DOMAIN}")
                                        build job: 'provisioning/pipeline-ipaclient-resignation',
                                            parameters: [
                                                string(
                                                    name: 'HOSTNAME', 
                                                    value: "${_HOSTNAME}"),
                                                string(
                                                    name: 'HOST_IP', 
                                                    value: "${_IPv4}"),
                                                string(
                                                    name: 'DOMAIN',
                                                    value: "${_DOMAIN}"),
                                                string(
                                                    name: 'REALM', 
                                                    value: "${_REALM}"),
                                            ],
                                            propagate: true,
                                            wait: true
                                        }
                                } catch (Exception e) {
                                    println("NAME RESOLUTION TEST FAILED: ${_HOSTNAME}.${_DOMAIN}")
                                }
                            }
                            parallel branches
                        }
                    }
                }
            }
        }

        stage('Plan destroy') {
            steps {
                dir("${workspace}/${REPO_DIR}") {
                    script {
                        PLAN_PARAMS = "plan --out '${env.BUILD_NUMBER}.tfbin' --destroy"
                        sh """
                        ${TF_VARS} terraform ${PLAN_PARAMS}
                        """
                    }
                }
            }
        }

        stage('Apply destroy') {
            steps {
                dir("${workspace}/${REPO_DIR}") {
                    script {                    
                        sh """
                        terraform apply '${env.BUILD_NUMBER}.tfbin'
                        """
                    }
                }
            }
        }

        stage('Update SCM') {
            environment {
                GIT_SSH_COMMAND = "ssh -o StrictHostKeyChecking=no"
            }
            steps {
                dir("${workspace}/${REPO_DIR}") {
                    script {
                        catchError(buildResult: 'SUCCESS', stageResult: 'ABORTED') {
                            sshagent([GIT_CREDS.toString()]) {
                                // DIFF FILES
                                _GIT_FILE_LIST = sh(
                                    script: "git diff-files --name-only",
                                    returnStdout: true
                                )
                                for (_FILE in _GIT_FILE_LIST.split("\n")) {
                                    sh("git add ${_FILE}")
                                }
                                // NEW FILES
                                _GIT_FILE_LIST = sh(
                                    script: "git ls-files . --exclude-standard --others",
                                    returnStdout: true
                                )
                                for (_FILE in _GIT_FILE_LIST.split("\n")) {
                                    sh("git add ${_FILE}")
                                }
                                sh("git commit -m 'PIPELINE: ${env.JOB_NAME} BUILD: ${env.BUILD_NUMBER}'")
                                sh("GIT_SSH_COMMAND='ssh -o StrictHostKeyChecking=no' git push origin HEAD:main")
                            }
                        }
                    }
                }
            }
        }
    }


    post {
        always {
            script {
                BUILD_USER = getBuildUser()
                COLOR_MAP = [
                'SUCCESS': 'good',
                'FAILURE': 'danger',
                ]
            }
        slackSend channel: params._SLACK_CHANNEL,
            color: COLOR_MAP[currentBuild.currentResult],
            message: """
            *${currentBuild.currentResult}:* Job ${env.JOB_NAME} build ${env.BUILD_NUMBER} 
            More info at: ${env.BUILD_URL}
            GIT_URL: ${env.GIT_URL}
            GIT_BRANCH: ${env.GIT_BRANCH}
            GIT_COMMIT: ${env.GIT_COMMIT}
            More info at: ${env.BUILD_URL}
            """
        }
        cleanup {
            cleanWs()
        }
    }
}

String getBuildUser() {
    ID = currentBuild.getBuildCauses('hudson.model.Cause$UserIdCause').userId
    if (ID) {
      return ID
    }
    else {
      return '[cronTrigger: */5 * * * *]'
    }
}